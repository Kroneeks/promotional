'use strict';

const body = document.querySelector('body'),
  formWrapper = document.getElementById('popupFormWrapper'),
  maxPreviewElements = 2,
  maxStockElements = 1,
  previewsElements = [
    `preview-page-`,
    `job-title-`,
    `preview-img-`
  ],
  stocksElements = [
    `stock-description-`,
    `stock-page-`,
    `stock-img-`
  ],
  activeIndexes = {
    previewActiveElement: 0,
    stockActiveElement: 0,
  };

let mobileNavBarActive = false;

document.addEventListener(`DOMContentLoaded`, () => {
  multiItemSlider();

  document.getElementById(`slider-pages-container`)
    .addEventListener('click', e => {
      changingActiveSlidesElementsByPager(e, previewsElements,
        "preview", activeIndexes);
    });

  document.getElementById(`stock-slider-pager`)
    .addEventListener('click', e => {
      changingActiveSlidesElementsByPager(e, stocksElements,
        "stock", activeIndexes);
    });

  document.getElementById(`preview-slider-controls`)
    .addEventListener(`click`, e => {
      changingActiveElementsByButtonControls(e, "preview", activeIndexes);
    });

  document.getElementById(`stocks-slider-controls`)
    .addEventListener(`click`, e => {
      changingActiveElementsByButtonControls(e, "stock", activeIndexes);
    });

  document.getElementById('footer-phone')
    .addEventListener('input', () => {
      autoCompletePhoneNumber('footer-phone');
    });

  document.getElementById('slowlyScrollTop')
    .addEventListener('click', () => {
      slowlyScroll();
    });

  document.getElementById('btnSetReview')
    .addEventListener('click', () => {
      showPopupForm();
    });

  document.getElementById('popupFormWrapper')
    .addEventListener('mousedown', e => {
      if (e.target.id === 'popupFormWrapper') {
        hidePopupForm();
      }
    });

  document.getElementById('btnClosePopupForm')
    .addEventListener('click', () => {
      hidePopupForm();
    });

  document.getElementById('mobileNavButton')
    .addEventListener('click', () => {
      showAndHideMobileNavBar();
    });

  document.getElementById('menuExternalArea')
    .addEventListener('click', () => {
      showAndHideMobileNavBar();
    })

});

function changingActiveSlidesElementsByPager(event, itemNames, itemGroup, activeItems) {

  const currentVisibleElement = event.target.id,
    targetIndex = +currentVisibleElement[currentVisibleElement.length - 1];

  if (isFinite(targetIndex)) {

    if (itemGroup === `preview`) {
      changingClassesOfElements(itemNames, activeItems.previewActiveElement,
        targetIndex);

      activeItems.previewActiveElement = targetIndex;
    }
    else if (itemGroup === `stock`) {
      changingClassesOfElements(itemNames, activeItems.stockActiveElement,
        targetIndex);

      activeItems.stockActiveElement = targetIndex;
    }

  }

}

function changingActiveElementsByButtonControls(event, itemGroup, activeItems) {

  const direction = event.target.id.replace(`${itemGroup}-control-`, ``);
  const currentElements = itemGroup === `preview` ?
    previewsElements : stocksElements;
  const maxCurrentActiveItem = itemGroup === `preview` ?
    maxPreviewElements : maxStockElements;
  let indexCurrentActiveItem = itemGroup === `preview` ?
    activeItems.previewActiveElement : activeItems.stockActiveElement;

  if (direction === "left") {

    if (indexCurrentActiveItem === 0) {
      changingClassesOfElements(currentElements,
        indexCurrentActiveItem, maxCurrentActiveItem);
      activeItems[`${itemGroup}ActiveElement`] = maxCurrentActiveItem;
    }
    else {
      changingClassesOfElements(currentElements,
        indexCurrentActiveItem, indexCurrentActiveItem - 1);

      activeItems[`${itemGroup}ActiveElement`] = indexCurrentActiveItem - 1;
    }

  }
  else if (direction === "right") {

    if (indexCurrentActiveItem === maxCurrentActiveItem) {
      changingClassesOfElements(currentElements,
        indexCurrentActiveItem, 0);

      activeItems[`${itemGroup}ActiveElement`] = 0;
    }
    else {
      changingClassesOfElements(currentElements,
        indexCurrentActiveItem, indexCurrentActiveItem + 1);

      activeItems[`${itemGroup}ActiveElement`] = indexCurrentActiveItem + 1;
    }

  }

}

function changingClassesOfElements(names, activeIndex, targetIndex) {

  for (let i = 0; i < 3; i++) {
    document
      .getElementById(`${names[i]}${activeIndex}`)
      .classList.toggle(`active`);
    document
      .getElementById(`${names[i]}${targetIndex}`)
      .classList.toggle(`active`);
  }

}

function autoCompletePhoneNumber(id) {
  document.getElementById(id).value = document
    .getElementById(id).value.replace(/(\d{3})\-?(\d{3})\-?(\d{4})/, '$1-$2-$3');
}

function slowlyScroll() {

  let currentYPosition = () => self.pageYOffset;

  let elmYPosition = (eId) => {
    let elm = document.getElementById(eId),
      y = elm.offsetTop,
      node = elm;
    while (node.offsetParent && node.offsetParent != document.body) {
      node = node.offsetParent;
      y += node.offsetTop;
    } return y;
  }


  let smoothScroll = (eId) => {
    let startY = currentYPosition(),
      stopY = elmYPosition(eId),
      distance = stopY > startY ? stopY - startY : startY - stopY;

    if (distance < 100) {
      scrollTo(0, stopY);
      return;
    }

    let speed = Math.round(distance / 200),
      step = Math.round(distance / 25),
      leapY = stopY > startY ? startY + step : startY - step,
      timer = 0;

    if (stopY > startY) {
      for (let i = startY; i < stopY; i += step) {
        setTimeout("window.scrollTo(0, " + leapY + ")", timer * speed);
        leapY += step; if (leapY > stopY) leapY = stopY; timer++;
      }
      return;
    }

    for (let i = startY; i > stopY; i -= step) {
      setTimeout("window.scrollTo(0, " + leapY + ")", timer * speed);
      leapY -= step; if (leapY < stopY) leapY = stopY; timer++;
    }
  }

  smoothScroll('pageHeader');
}

function showPopupForm() {
  formWrapper.style.display = 'block';
  body.style.overflow = 'hidden';
  if (window.innerWidth > 768) {
    body.style.marginRight = '15px';
  }
}

function hidePopupForm() {
  console.log(1);
  formWrapper.style.display = 'none';
  body.style.overflow = 'visible';
  if (window.innerWidth > 768) {
    body.style.marginRight = '0px';
  }
}

function showAndHideMobileNavBar() {
  const navButton = document.querySelector('.mobile-nav-button'),
    externalArea = document.querySelector('.menu-external-area'),
    navBar = document.querySelector('.header-middle-area');
  if (mobileNavBarActive) {
    navButton.style.right = '0px';
    navButton.style.backgroundImage = 'url(../img/menu.svg)';
    navButton.style.backgroundSize = '40%';
    navButton.style.boxShadow = 'rgba(0, 0, 0, 0.5) 0px 0px 15px';
    navBar.style.right = '-300px';
    navBar.style.boxShadow = 'none';
    externalArea.style.visibility = 'hidden';
    body.style.overflow = 'visible';
    body.style.marginRight = '0px'
  }
  else {
    navButton.style.right = '300px';
    navButton.style.backgroundImage = 'url(../img/closeWhite.svg)';
    navButton.style.backgroundSize = '30%';
    navButton.style.boxShadow = 'none';
    navBar.style.right = '0px';
    navBar.style.boxShadow = 'rgba(0, 0, 0, 0.5) 0px 0px 15px';
    externalArea.style.visibility = 'visible';
    body.style.overflow = 'hidden';
    body.style.marginRight = '15px'
  }
  mobileNavBarActive = !mobileNavBarActive;
}

function multiItemSlider() {

  const mainElement = document.querySelector('.review'),
    sliderWrapper = mainElement.querySelector('.reviews-slider-wrapper'),
    sliderItems = mainElement.querySelectorAll('.slider-item'),
    sliderControls = mainElement.querySelectorAll('.reviews-controls'),
    wrapperWidth = parseFloat(getComputedStyle(sliderWrapper).width),
    itemWidth = parseFloat(getComputedStyle(sliderItems[0]).width),
    step = itemWidth / wrapperWidth * 100;

  let positionLeftItem = 0,
    currentTransform = 0,
    items = [];

  sliderItems.forEach((item, index) => {
    items.push({ item: item, position: index, transform: 0 });
  });

  let position = {
    getItemMin: function () {
      let indexItem = 0;
      items.forEach((item, index) => {
        if (item.position < items[indexItem].position) {
          indexItem = index;
        }
      });
      return indexItem;
    },
    getItemMax: function () {
      let indexItem = 0;
      items.forEach((item, index) => {
        if (item.position > items[indexItem].position) {
          indexItem = index;
        }
      });
      return indexItem;
    },
    getMin: function () {
      return items[position.getItemMin()].position;
    },
    getMax: function () {
      return items[position.getItemMax()].position;
    }
  };

  let transformItem = direction => {
    let nextItem;

    if (direction === 'right') {
      positionLeftItem++;

      if ((positionLeftItem + wrapperWidth / itemWidth - 1) > position.getMax()) {
        nextItem = position.getItemMin();
        items[nextItem].position = position.getMax() + 1;
        items[nextItem].transform += items.length * 100;
        items[nextItem].item.style.transform = 'translateX(' + items[nextItem].transform + '%)';
      }
      currentTransform -= step;
    }

    if (direction === 'left') {
      positionLeftItem--;
      if (positionLeftItem < position.getMin()) {
        nextItem = position.getItemMax();
        items[nextItem].position = position.getMin() - 1;
        items[nextItem].transform -= items.length * 100;
        items[nextItem].item.style.transform = 'translateX(' + items[nextItem].transform + '%)';
      }
      currentTransform += step;
    }

    sliderWrapper.style.transform = 'translateX(' + currentTransform + '%)';
  };

  let ControlClick = e => {
    let direction = e.target.classList
      .contains(`review-control-right`) ? 'right' : 'left';
    e.preventDefault();
    transformItem(direction);
  };

  let setUpListeners = () => {
    sliderControls.forEach(item => item.addEventListener('click', ControlClick));
  };

  setUpListeners();

  return {
    right: function () {
      transformItem('right');
    },
    left: function () {
      transformItem('left');
    }
  }
}